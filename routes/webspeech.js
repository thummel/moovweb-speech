var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('webspeech', { title: 'Web Speech API' });
});

module.exports = router;