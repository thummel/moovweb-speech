var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('googlespeech', { title: 'Google Speech API' });
});

module.exports = router;