var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

if (SpeechRecognition && SpeechGrammarList && SpeechRecognitionEvent) {

  var colors = [ 'aqua' , 'azure' , 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral', 'crimson', 'cyan', 'fuchsia', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green', 'indigo', 'ivory', 'khaki', 'lavender', 'lime', 'linen', 'magenta', 'maroon', 'moccasin', 'navy', 'olive', 'orange', 'orchid', 'peru', 'pink', 'plum', 'purple', 'red', 'salmon', 'sienna', 'silver', 'snow', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'white', 'yellow'];
  var whiteText = [ 'black', 'blue', 'brown', 'gray', 'green', 'indigo', 'maroon', 'navy', 'purple', 'sienna', 'teal' ];
  var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;';
  
  var recognition = new SpeechRecognition();
  var speechRecognitionList = new SpeechGrammarList();
  speechRecognitionList.addFromString(grammar, 1);
  recognition.grammars = speechRecognitionList;
  //recognition.continuous = false;
  recognition.lang = 'en-US';
  recognition.interimResults = false;
  recognition.maxAlternatives = 1;
  
  var diagnostic = document.querySelector('.output');
  var hints = document.querySelector('.hints');
  var box = document.querySelector('#color-box');
  var activateMicrophone = document.querySelector('#activate-microphone');
  
  var colorHTML= '<ul>';
  colors.forEach(function(v, i, a){
    // console.log(v, i);
    var styles = 'background-color:' + v + ';';
    if (whiteText.indexOf(v) > -1) {
      styles += 'color: #ddd;';
    }
    colorHTML += '<li style="' + styles + '"><div class="cell">' + v + '</div></li>';
  });
  colorHTML += '</ul>';
  hints.innerHTML = colorHTML;
  
  activateMicrophone.onclick = function() {
    activateMicrophone.disabled = true;
    recognition.start();
    diagnostic.textContent = 'Ready to receive a color command...';
  }
  
  recognition.onresult = function(event) {
    // The SpeechRecognitionEvent results property returns a SpeechRecognitionResultList object
    // The SpeechRecognitionResultList object contains SpeechRecognitionResult objects.
    // It has a getter so it can be accessed like an array
    // The [last] returns the SpeechRecognitionResult at the last position.
    // Each SpeechRecognitionResult object contains SpeechRecognitionAlternative objects that contain individual results.
    // These also have getters so they can be accessed like arrays.
    // The [0] returns the SpeechRecognitionAlternative at position 0.
    // We then return the transcript property of the SpeechRecognitionAlternative object
  
    var last = event.results.length - 1;
    var color = event.results[last][0].transcript;
  
    diagnostic.textContent = 'Result received: ' + color;
  
    box.style.backgroundColor = color;
  
    activateMicrophone.disabled = false
  
    console.log('Confidence: ' + event.results[0][0].confidence);
  }
  
  recognition.onspeechend = function() {
    recognition.stop();
  }
  
  recognition.onnomatch = function(event) {
    diagnostic.textContent = "I didn't recognise that color";
  }
  
  recognition.onerror = function(event) {
    diagnostic.textContent = 'Error occurred in recognition: ' + event.error;
  }
  
} else {
  errorContainer.textContent = 'Sorry, this browser does not support the web speech api.';
}